﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Fijo.Tools.Gedit.Quickstart {
	class Program {
		const int SW_HIDE = 0;
		const int SW_SHOWNORMAL = 1;
		const int SW_SHOW = 5;

		[DllImport("user32.dll")]
		static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);

		static void Main(string[] args) {
			if(args.Length == 0) {
				var lines = File.ReadAllLines(@"R:\var\gedit\handles");
				while(lines.Length == 0) {
					Init();
					lines = File.ReadAllLines(@"R:\var\gedit\handles");
				}
				var index = 0;
				var success = false;
				foreach (var line in lines) {
					success = ShowWindow(new IntPtr(long.Parse(line)), SW_SHOWNORMAL);
					File.WriteAllLines(@"R:\var\gedit\handles", lines.Skip(++index).ToArray());
					if(success) break;
				}
				if(!success) Main(args);
				var updateLines = File.ReadAllLines(@"R:\var\gedit\handles");
				if(updateLines.Length == 0) updateLines = new string[1];
				updateLines[0] = GetPreloadedHandleLine();
				File.WriteAllLines(@"R:\var\gedit\handles", updateLines);
			}
			else switch(args[0]) {
				case "init":
					MayCreateDir(@"R:\var\");
					MayCreateDir(@"R:\var\gedit");
					Init();
					break;
			}
		}

		private static void MayCreateDir(string path) {
			if(!Directory.Exists(path)) Directory.CreateDirectory(path);
		}

		private static void Init() {
			File.WriteAllLines(@"R:\var\gedit\handles", Enumerable.Range(0, 64).AsParallel().Select(x => GetPreloadedHandleLine()));
		}

		private static string GetPreloadedHandleLine() {
			return PreloadNew().ToInt64().ToString();
		}

		static IntPtr PreloadNew() {
			var process = GetProcess();
			process.Start();
			var mainWindowHandle = GetMainWindowHandle(process);
			ShowWindow(mainWindowHandle, SW_HIDE);
			return mainWindowHandle;
		}

		static IntPtr GetMainWindowHandle(Process process) {
			IntPtr mainWindowHandle;
			do Thread.Sleep(1); while ((mainWindowHandle = process.MainWindowHandle).Equals(IntPtr.Zero));
			return mainWindowHandle;
		}

		static Process GetProcess() {
			return new Process {StartInfo = GetStartInfo()};
		}

		static ProcessStartInfo GetStartInfo() {
			return new ProcessStartInfo
			       {
				       FileName = @"C:\Program Files (x86)\gedit\bin\gedit.exe",
				       WorkingDirectory = @"C:\Program Files (x86)\gedit\bin",
				       CreateNoWindow = false,
				       RedirectStandardError = false,
				       RedirectStandardInput = false,
				       RedirectStandardOutput = false,
					   UseShellExecute = true,
					   WindowStyle = ProcessWindowStyle.Minimized
			       };
		}
	}
}
